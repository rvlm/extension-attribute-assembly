﻿Extension methods assembly for .NET Framework 2.0
=================================================

This is the assembly containing only one class `ExtensionAttribute` located in
namespace `System.Runtime.CompilerServices`. This is the helper class which
allows us to bring support for extension methods feature of C# language to
old runtime (version 2.0). So, the project contains only one effective code
file which was taken from LinqBridge project without modifications, the rest
of project content (like assembly metadata information or folder structure)
is rather trivial.

The only one reason to bring this project to life was the fact that there are
many different projects reinventing the extension methods hack, which leads to
conflicts on compilation. See project wiki for more information.
