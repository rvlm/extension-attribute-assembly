@echo off
set SOLUTION=ExtensionAttributeAssembly.sln
set CONFIGURATION=Release
set OUTPUTDIR=build

set VARIANT=default
set FRAMEWORK=v2.0
set CONSTANTS=
call :build

call :pack

pause
exit

:build
echo *** Building solution variant %VARIANT% ...
msbuild %SOLUTION%                         ^
    /verbosity:quiet                       ^
    /t:Rebuild                             ^
    /p:Configuration=%CONFIGURATION%       ^
    /p:TargetFrameworkVersion=%FRAMEWORK%  ^
    /p:DefineConstants=%CONSTANTS%         ^
	/p:OutputPath=..\%OUTPUTDIR%\%VARIANT%
goto :EOF

:pack
echo *** Packing solution into NuGet package ...
nuget pack %SOLUTION%.nuspec -OutputDirectory %OUTPUTDIR%
goto :EOF
